import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import AdminPage from './Screens/Admin/AdminPage';
import HomePage from './Screens/Home/HomePage';
import NotFoundPage from './Screens/NotFound/NotFoundPage';
import LayoutAdmin from './Template/AdminTemplate/LayoutAdmin';
import LayoutHome from './Template/HomeTemplate/LayoutHome';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/Home' element={<LayoutHome Component={HomePage}/>}></Route>
      <Route path='/Admin' element={<LayoutAdmin Component={AdminPage}/>}></Route>

      {/* notfound page */}
      <Route path='*' element={<NotFoundPage/>}></Route>
    </Routes>
    </BrowserRouter>
  );
}

export default App;
