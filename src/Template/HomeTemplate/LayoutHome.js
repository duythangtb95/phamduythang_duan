import React from 'react'
import Footer from '../../Components/Footer/Footer'
import Header from '../../Components/Header/Header'

export default function LayoutHome({Component}) {
  return (
    <div> 
        <Header/>
        <Component/>
        <Footer/>
    </div>
  )
}
